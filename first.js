// game guess the number
var number = Math.floor(Math.random() * 5) + 1;
console.log(number);
var playOrNotChoice = confirm("Do you want play this game?");
if (playOrNotChoice === false) {
    alert('Not today');
} else {
    alert("You have 3 attempts.Enter number between 1 and 5");
    var attempt;
    for (attempt = 2; attempt >= 0; attempt--) {
        var userNumber = prompt("Number", "Enter number");
        if (userNumber == number) {
            alert('Congratulations! You are winner!');
            break;
        }
        while (userNumber != number) {
            if (attempt == 0) {
                alert("You lose a game!");
            } else {
                alert("You have " + attempt + " attempts, try again!");
            }
            break;
        }
    }
}
// Rewrite while to for
function myFunction() {
    let i;
    for (i = 1; i < 10; i++) {
        console.log(i);
        i++;
    }
};

//function using loop for found all prime numbers (prime(min, max))
function prime(min, max) {
    var arr = new Array();
    for (var i = min; i <= max; i++) {
        arr.push(i);
    }
    var minPrimeNumber = 2;
    while (minPrimeNumber <= Math.sqrt(max)) {
        for (var k = 0; k < arr.length; k++) {
            if (arr[k] == 1) {
                arr.splice(k, 1);
            }
            if (arr[k] % minPrimeNumber == 0 && arr[k] !== minPrimeNumber) {
                arr.splice(k, 1);
            }
        }
        minPrimeNumber++;
    }
    console.log(arr);
}
